<?php

return [
    'enabled' => env('QUEUE_MONITOR_ENABLED', true),

    'queues' => [
        'default'
    ],

    'silenced_jobs' => [
        'App\\Jobs\\TestJob'
    ],

    'ignored_jobs' => [

    ],

    'connection' => env('DB_CONNECTION', 'mysql'),
];
