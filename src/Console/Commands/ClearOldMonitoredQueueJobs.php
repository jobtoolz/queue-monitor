<?php

namespace QueueMonitor\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use QueueMonitor\QueueMonitor;

class ClearOldMonitoredQueueJobs extends Command
{
    protected $signature = 'queue-monitor:clear';

    protected $description = 'Removes stale queued monitored jobs from database.';

    public function handle()
    {
        QueueMonitor::table()
            ->where('started_at', '<', now()->subDays(7)->timestamp * 1000)
            ->delete();
    }
}