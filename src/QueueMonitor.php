<?php

namespace QueueMonitor;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class QueueMonitor
{
    public static function save(array $payload)
    {
        try {
            self::table()
                ->updateOrInsert(['id' => $payload['id']], $payload);
        } catch (\Exception $e) {
            Log::error($e);
        }
    }

    public static function table()
    {
        return DB::connection(config('queue_monitor.connection'))
            ->table('monitored_queue_jobs');
    }
}
