<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function getConnection()
    {
        return config('queue_monitor.connection');
    }
    
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('monitored_queue_jobs', function (Blueprint $table) {
            $table->bigInteger('pushed_at')->after('php')->nullable();
            $table->dropColumn('payload');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        
    }
};
